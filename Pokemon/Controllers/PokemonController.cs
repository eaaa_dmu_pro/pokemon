﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Pokemon.Models;

namespace Pokemon.Controllers
{
    public class PokemonController : Controller
    {
        // GET: Pokemon
        public async Task<ActionResult> Index()
        {
            using (var client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync("https://api.pokemontcg.io/v1/sets?standardLegal=true");
                if (response.IsSuccessStatusCode)
                {
                    PokemonSets pokemonSets = await response.Content.ReadAsAsync<PokemonSets>();
                    return View(pokemonSets.sets);
                }
            }
            return new HttpNotFoundResult();
        }

        public async Task<ActionResult> Set(string set)
        {
            using (var client = new HttpClient())
            {
                UriBuilder uriBuilder = new UriBuilder("https://api.pokemontcg.io/v1/cards");
                uriBuilder.Query = "supertype=Pokémon&setCode=" + set;
                HttpResponseMessage response = await client.GetAsync(uriBuilder.Uri);
                if (response.IsSuccessStatusCode)
                {
                    PokemonCards cards = await response.Content.ReadAsAsync<PokemonCards>();
                    return View(cards.cards);
                }
            }
            return new HttpNotFoundResult();
        }
    }
}