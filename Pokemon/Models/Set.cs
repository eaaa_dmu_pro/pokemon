﻿using System.Collections.Generic;

namespace Pokemon.Models
{
    public class PokemonSets
    {
        public List<PokemonSet> sets { get; set; }
    }
    public class PokemonSet
    {
        public string code { get; set; }
        public string name { get; set; }
        //public string series { get; set; }
        public int totalCards { get; set; }
        public bool standardLegal { get; set; }
        public bool expandedLegal { get; set; }
        public string releaseDate { get; set; }
    }
}
